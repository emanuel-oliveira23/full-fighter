package game.fullfighter.main;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

import game.fullfighter.main.Character.CharacterDirection;
import game.fullfighter.main.Character.CharacterListener;
import game.fullfighter.main.PlayerLayer.PlayerLayerListener;
import game.fullfighter.res.Resources;

public class GameBackground extends GameObject 
	implements CharacterListener, PlayerLayerListener {
	
	public static final int MARGIN_TOP = 50;
	
	private BufferedImage backgroundImage;
	private int px = 448;
	private int py = 0;
	private int wi = 0;
	private int hi = 0;
	private int backgroundSpeed = 10;
	
	private int nextPlayerXLimit = Constants.SCREEN_WIDTH;
	
	private BackgroundLayerListener listener;
	private boolean canGo = false;
	
	public GameBackground() {
		initBakcground();
	}
	
	private void initBakcground() {
		backgroundImage = Resources.getImage("bg-scene.png");
		wi = backgroundImage.getWidth();
		hi = backgroundImage.getHeight();	
	}
	
	public void setListener(BackgroundLayerListener listener) {
		this.listener = listener;
	}
	
	@Override
	public void update(Graphics2D g) {
		if (px + Constants.SCREEN_WIDTH > wi) {
			px = wi - Constants.SCREEN_WIDTH;
		}
		Image subImage = backgroundImage.getSubimage(px, py, Constants.SCREEN_WIDTH, hi);
		g.drawImage(subImage, 0, MARGIN_TOP, Constants.SCREEN_WIDTH, hi, null);
		
		if (canGo) {
			helpToGo(g);
		}
	}
	
	private boolean toogleHelpGo;
	public void helpToGo(Graphics2D g) {
		if (!toogleHelpGo) {
			toogleHelpGo = true;
			int x = Constants.SCREEN_WIDTH - 150;
			int y = Constants.MIN_CHARACTER_Y;
			g.setColor(Color.BLACK);
			g.setFont(new Font(Font.SANS_SERIF, Font.BOLD | Font.ITALIC, 64));
			g.drawString("Go> ", x + 5, y + 5);
			g.setColor(Color.YELLOW);
			g.drawString("Go> ", x, y);
		} else {
			toogleHelpGo = false;
		}
	}

	@Override
	public void onChangePosition(int x, int y, CharacterDirection direction) {
		if (canGo){
			if ((x + px) > (nextPlayerXLimit - Constants.SCREEN_WIDTH/2) || 
					px >= (wi - Constants.SCREEN_WIDTH) ) {
				canGo = false;
				if (listener != null) listener.onEndCanGo();
			} else {
				if (listener != null) listener.dragPlayer();
			}
		}
		
		if (canGo && direction == CharacterDirection.RIGHT) {
			px += backgroundSpeed;
		}
		
	}
	
	@Override
	public void onKillEnemies() {
		canGo = true;
		
		nextPlayerXLimit += Constants.SCREEN_WIDTH;
		
		if (nextPlayerXLimit > wi) {
			nextPlayerXLimit = (wi - Constants.SCREEN_WIDTH);
		}
	}
	
	public static interface BackgroundLayerListener {
		
		public void dragPlayer();
		public void onEndCanGo();
	}
	
}
