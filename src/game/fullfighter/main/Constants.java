package game.fullfighter.main;

public class Constants {

	public static final int SCREEN_WIDTH = 800;
	public static final int SCREEN_HEIGHT = 275;
	public static final int MIN_CHARACTER_Y = GameBackground.MARGIN_TOP + 130;
	public static final int MAX_CHARACTER_Y = GameBackground.MARGIN_TOP + 186;
	
}
