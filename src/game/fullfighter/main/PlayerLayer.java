package game.fullfighter.main;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import game.fullfighter.main.GameBackground.BackgroundLayerListener;

public class PlayerLayer extends Layer implements BackgroundLayerListener {

	private Player player;
	private List<List<Enemy>> enemies = new ArrayList<>();
	private List<Enemy> currentEnemies = Collections.emptyList();
	private int currentEnemiesKey = 0;
	
	private PlayerLayerListener listener;
	private boolean listenerHasBeenCalled = false;
	private boolean dragPlayer = false;
	
	public PlayerLayer(Player player) {
		this.player = player;
		initEnemies();
	}
	
	private void initEnemies() {
		int x = Constants.SCREEN_WIDTH - 100;
		int x2 = 100;
		int y1 = Constants.MAX_CHARACTER_Y;
		int y2 = Constants.MIN_CHARACTER_Y;
		int y3 = (y1 + y2) / 2;
		
		
		enemies.add(enemiesList(
			new Enemy(x, y1, 4, 5, player),
			new Enemy(x, y2, 4, 5, player)));
		
		enemies.add(enemiesList(
				new Enemy(x, y1, 5, 6, player),
				new Enemy(x2, y2, 5, 6, player)));
		
		enemies.add(enemiesList(
				new Enemy(x, y1, 7, 8, player),
				new Enemy(x2, y2, 7, 8, player),
				new Enemy(x, y3, 7, 8, player)));
		
		enemies.add(enemiesList(
				new Enemy(x, y1, 8, 9, player),
				new Enemy(x2, y2, 8, 9, player),
				new Enemy(x2, y3, 8, 9, player)));
		
		enemies.add(enemiesList(
				new Enemy(x, y1, 8, 9, player),
				new Enemy(x, y2, 8, 9, player),
				new Enemy(x2, y3, 8, 9, player)));
		
		enemies.add(enemiesList(
				new Enemy(x2, y1, 8, 9, player),
				new Enemy(x, y1, 8, 9, player),
				new Enemy(x, y2, 8, 9, player),
				new Enemy(x2, y3, 8, 9, player)));
		
		currentEnemies = enemies.get(0);
	}
	
	private List<Enemy> enemiesList(Enemy ...enemies) {
		List<Enemy> enemiesList = new ArrayList<>();
		for (Enemy e: enemies) {
			enemiesList.add(e);
		}
		return enemiesList;
	}
	
	public boolean allEnemiesKilled() {
		boolean res = true;
		for (List<Enemy> e : enemies) {
			if (!e.isEmpty()) {
				res = false;
				break;
			}
		}
		return res;
	}
	
	public void nextEnemies() {
		currentEnemiesKey++;
		if (currentEnemiesKey < enemies.size()) {
			currentEnemies = enemies.get(currentEnemiesKey);
		}
	}
	
	public void setListener(PlayerLayerListener listener) {
		this.listener = listener;
	}
	
	@Override
	public void draw(Graphics2D g) {
		List<Character> characters = new ArrayList<>(currentEnemies);
		characters.add(player);
		Collections.sort(characters, new Comparator<Character>() {
			@Override
			public int compare(Character c1, Character c2) {
				return c1.getY() - c2.getY();
			}
		} );
		
		if (dragPlayer) {
			dragPlayer();
		}
		
		for (Character c : characters) {
			if (c.died() && c != player) {
				currentEnemies.remove(c);
				c.die();
			}
			c.update(g);
		}
		
		checkColisions();
		animateCharacters();
		
		checkEnemies();
	}
	
	private void checkColisions() {
		for (Enemy enemy : currentEnemies) {
			//check if player attack enemy
			Rectangle playerAttackBox = player.getAttackBox();
			Rectangle enemyHitBox = enemy.getHitBox();
			
			if (playerAttackBox != null && enemyHitBox != null && 
					playerAttackBox.intersects(enemyHitBox)) {
				if (player.getCurrentState().equals("superPunch")) {
					enemy.die();
				} else {
					enemy.hit();
				}
				break;
			}
			
			//check if enemy attack player
			Rectangle playerHitBox = player.getHitBox();
			Rectangle enemyAttackBox = enemy.getAttackBox();
			
			if (playerHitBox != null && enemyAttackBox != null && 
					enemyAttackBox.intersects(playerHitBox)) {
				if (enemy.getCurrentState().equals("superPunch")) {
					player.die();
				} else {
					player.hit();
				}
			}
		}
	}
	
	private void animateCharacters() {
		player.nextAnimation();
		
		for(Enemy e : currentEnemies) {
			e.nextAnimation();
		}
	}
	
	private void checkEnemies() {
		if (currentEnemies.isEmpty() && !allEnemiesKilled()){
			if(!listenerHasBeenCalled && listener != null) {
				listener.onKillEnemies();
				listenerHasBeenCalled = true;
			}
		}
	}
	
	@Override
	public void onEndCanGo() {
		dragPlayer = true;
		listenerHasBeenCalled = false;
		nextEnemies();
	}
	
	@Override
	public void dragPlayer() {
		if (player.getX() > (150 + Constants.SCREEN_WIDTH / 2)) {
			player.setX(player.getX() - 25);
		} else {
			dragPlayer = false;
		}
	}
	
	public static interface PlayerLayerListener {
		
		public void onKillEnemies();
		
	}

}
