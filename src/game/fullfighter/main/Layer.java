package game.fullfighter.main;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

public class Layer {

	private List<GameObject> objects = new ArrayList<GameObject>();
	
	public void addGameObject(GameObject gameObject) {
		objects.add(gameObject);
	}
	
	public void removeGameObject(GameObject gameObject) {
		int idx = objects.indexOf(gameObject);
		if (idx != -1) {
			objects.remove(idx);
		}
	}
	
	public void draw(Graphics2D g) {
		for (GameObject go : objects ) {
			go.update(g);
		}
	}
	
}
