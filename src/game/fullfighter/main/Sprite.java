package game.fullfighter.main;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import game.fullfighter.res.Resources;

public class Sprite {
	
	private BufferedImage spriteSheet;
	private int numRows, numCols;
	private int centerX, centerY;
	private int pieceWidth, pieceHeight;
	
	public Sprite(String spriteResource, int numRows, int numCols, int centerX, int centerY) {
		this.numRows = numRows;
		this.numCols = numCols;
		this.centerX = centerX;
		this.centerY = centerY;
		loadSpriteSheet(spriteResource);
	}
	
	private void loadSpriteSheet(String spriteSource) {
		try {
			spriteSheet = ImageIO.read(Resources.getResourceAsStream(spriteSource));
			pieceWidth = spriteSheet.getWidth() / numCols;
			pieceHeight = spriteSheet.getHeight() / numRows;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public BufferedImage getPiece(int row, int col) {
		int x = col * pieceWidth;
		int y = row * pieceHeight;
		return spriteSheet.getSubimage(x, y, pieceWidth, pieceHeight);
	}
	
	public int getNumRows() {
		return numRows;
	}
	
	public int getNumCols() {
		return numCols;
	}
	
	public int getCenterX() {
		return centerX;
	}
	
	public int getCenterY() {
		return centerY;
	}
}
