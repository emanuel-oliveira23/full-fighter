package game.fullfighter.main;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GameInput {
	
	private static GameInput instance = null;
	
	private int lastKeyCode = -1;
	private KeyListener keyListener = new KeyListener() {
		
		@Override
		public void keyTyped(KeyEvent e) {}
		
		@Override
		public void keyReleased(KeyEvent e) {
			instance.lastKeyCode = -1;
		}
		
		@Override
		public void keyPressed(KeyEvent e) {
			lastKeyCode = e.getKeyCode();
		}
	};
	
	
	private GameInput() {
	
	}
	
	public KeyListener getKeyListener() {
		return keyListener;
	}
	
	public static GameInput getInstance() {
		if (instance == null) {
			instance = new GameInput();
		}
		return instance;
	}
	
	public static int getLastKeyCode() {
		return getInstance().lastKeyCode;
	}
	
	
}
