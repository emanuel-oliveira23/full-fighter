package game.fullfighter.main;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import game.fullfighter.main.BoundBox.BoundBoxType;

public abstract class Character extends GameObject 
	implements StateMachine.OnChangeStateListener {
	
	public static final int DEFAULT_CHARACTER_SPEED = 10;
	public static final int INITIAL_LIFE = 100;
	
	private Sprite spriteSheet;
	private int x, y;
	private int characterSpeed;
	private Map<String, SpriteAnimation> animations;
	private BufferedImage characterImage;
	private CharacterDirection direction = CharacterDirection.RIGHT;
	private List<CharacterListener> characterListeners;
	private StateMachine characterStateMachine;
	private int life;
	
	
	public Character(Sprite spriteSheet, int x, int y) {
		this.spriteSheet = spriteSheet;
		this.x = x;
		this.y = y;
		characterSpeed = DEFAULT_CHARACTER_SPEED;
		animations = getAnimations();
		characterListeners = new ArrayList<>();
		life = INITIAL_LIFE;
		
		initStateMachine();
	}
	
	protected void initStateMachine() {
		characterStateMachine = new StateMachine("idle");
		characterStateMachine.addTransaction("idle", "idle");
		characterStateMachine.addTransaction("idle", "walk");
		characterStateMachine.addTransaction("idle", "punch");
		characterStateMachine.addTransaction("idle", "superPunch");
		characterStateMachine.addTransaction("idle", "hit");
		characterStateMachine.addTransaction("idle", "die");
		characterStateMachine.addTransaction("walk", "walk");
		characterStateMachine.addTransaction("walk", "idle");
		characterStateMachine.addTransaction("walk", "punch");
		characterStateMachine.addTransaction("walk", "superPunc");
		characterStateMachine.addTransaction("walk", "hit");
		characterStateMachine.addTransaction("walk", "die");
		characterStateMachine.addTransaction("punch", "idle");
		characterStateMachine.addTransaction("punch", "walk");
		characterStateMachine.addTransaction("superPunch", "idle");
		characterStateMachine.addTransaction("superPunch", "walk");
		characterStateMachine.addTransaction("hit", "idle");
		characterStateMachine.addTransaction("hit", "walk");
		characterStateMachine.addTransaction("hit", "die");
		
		//TODO: Analisar se de die pode-se voltar para algum outro estado
		characterStateMachine.addTransaction("die", "idle");
		
		characterStateMachine.setOnChangeListener(this);
	}
	
	protected boolean setCurrentState(String targetState) {
		return animations.get(getCurrentState()).canBreak() && 
				characterStateMachine.to(targetState);
	}
	
	public String getCurrentState() {
		return characterStateMachine.getCurrentState();
	}
	
	@Override
	public void onChangeState(String newState, String oldState) {
		if (oldState != newState) {
			animations.get(oldState).reset();
		} else {
			SpriteAnimation animation = animations.get(newState);
			if (animation.isEnded()) {
				animation.reset();
			}
		}
	}
	
	private int getPx() {
		return x - getCenterX();
	}
	
	private int getPy() {
		return y - getCenterY();
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public int getWidth() {
		if (characterImage != null) return characterImage.getWidth();
		else return 0;
	}
	
	public int getHeight() {
		if (characterImage != null) return characterImage.getHeight();
		else return 0;
	}
	
	public int getCenterX() {
		return spriteSheet.getCenterX();
	}
	
	public int getCenterY() {
		return spriteSheet.getCenterY();
	}
	
	protected void setCharacterSpeed(int characterSpeed) {
		this.characterSpeed = characterSpeed;
	}
	
	protected int getCharacterSpeed() {
		return characterSpeed;
	}
	
	public CharacterDirection getDirection() {
		return direction;
	}
	
	public void setDirection(CharacterDirection direction) {
		this.direction = direction;
	}
	
	public int getLife() {
		return life;
	}
	
	public boolean died() {
		return life <= 0;
	}
	
	public Rectangle getBounds() {
		return new Rectangle(getPx(), getPy(), getWidth(), getHeight());
	}
	
	public Rectangle getHitBox() {
		return findBoundBox(BoundBoxType.HIT);
	}
	
	public Rectangle getAttackBox() {
		return findBoundBox(BoundBoxType.ATTACK);
	}
	
	private Rectangle findBoundBox(BoundBoxType type) {
		BoundBox[] characterBoundBoxies = animations.get(getCurrentState()).getCurrentBoundBoxies();
		BoundBox bbfound = null;
		
		for (BoundBox bb : characterBoundBoxies) {
			if (bb.is(type)) {
				bbfound = bb;
				break;
			}
		}
		
		if (bbfound != null) {
			int rx = (int) bbfound.getX() + getPx();
			int ry = (int) bbfound.getY() + getPy();
			int rWidth = (int) bbfound.getWidth();
			int rHeight = (int) bbfound.getHeight();
			
			if (getDirection() == CharacterDirection.LEFT) {
				rx = ((getWidth() - (int) bbfound.getX()) - rWidth) + getPx();
			}
			
			return new Rectangle(rx, ry, rWidth, rHeight);
		} else {
			return null;
		}
	}
	
	@Override
	public void update(Graphics2D g) {
		SpriteAnimation animation = animations.get(getCurrentState());
		if (animation.isEnded() && !setCurrentState(getCurrentState())) {
			idle();
		}
		
		int row = animation.getSpriteRow();
		int col = animation.getActualCol();
		characterImage = spriteSheet.getPiece(row, col);
		
		flipByDirection();
		g.drawImage(characterImage, getPx(), getPy(), null);
		drawLifeBar(g);
	}
	
	private void flipByDirection() {
		if (direction == CharacterDirection.LEFT) {
			AffineTransform tx = AffineTransform.getScaleInstance(-1, 1);
			tx.translate(-characterImage.getWidth(), 0);
			AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
			characterImage = op.filter(characterImage, null);
		}
	}
	
	protected void drawLifeBar(Graphics2D g) {
		int _life = life > 0 ? life : 0;
		g.setColor(Color.ORANGE);
		g.drawRect(getX() - INITIAL_LIFE / 2, getPy() - 30, INITIAL_LIFE, 20);
		g.setColor(new Color(255, 127, 0, 200));
		g.fillRect(getX() - INITIAL_LIFE / 2, getPy() - 30, _life, 20);
	}
	
	protected void moveToUp() {
		if (walk()) {
			y -= characterSpeed;
			if (y > Constants.MIN_CHARACTER_Y) {
				notifyPositionChange();
			} else {
				y += characterSpeed;
			}
		}
	}
	
	protected void moveToRight() {
		if (walk()) {
			x += characterSpeed;
			if (x <= Constants.SCREEN_WIDTH ) {
				notifyPositionChange();
			} else {
				x -= characterSpeed;
			}
			direction = CharacterDirection.RIGHT;
		}
	}
	
	protected void moveToDown() {
		if (walk()) {
			y += characterSpeed;
			if (y <= Constants.MAX_CHARACTER_Y ) {
				notifyPositionChange();
			} else {
				y -= characterSpeed;
			}
		}
	}
	
	protected void moveToLeft() {
		if (walk()) {
			x -= characterSpeed;
			if (x > 0) {
				notifyPositionChange();
			} else {
				x += characterSpeed;
			}
			direction = CharacterDirection.LEFT;
		}
	}
	
	protected boolean idle() {
		return setCurrentState("idle");
	}
	
	protected boolean walk() {
		return setCurrentState("walk");
	}
	
	protected boolean punch() {
		return setCurrentState("punch");
	}
	
	protected boolean superPunch() {
		return setCurrentState("superPunch");
	}
	
	protected boolean hit() {
		if(setCurrentState("hit")) {
			life -= hitLife();
			return true;
		} else {
			return false;
		}
	}
	
	protected boolean die() {
		if(setCurrentState("die")) {
			life -= dieLife();
			return true;
		} else {
			return false;
		}
	}
	
	public void nextAnimation() {
		animations.get(getCurrentState()).nextCol();
	}
	
	public void addCharacterListener(CharacterListener characterListener) {
		characterListeners.add(characterListener);
	}
	
	public void removeCharacterListenet(CharacterListener characterListener) {
		int index = characterListeners.indexOf(characterListener);
		if (index != -1) {
			characterListeners.remove(index);
		}
	}
	
	public void notifyPositionChange() {
		for (CharacterListener cl : characterListeners) {
			cl.onChangePosition(x, y, direction);
		}
	}
	
	protected abstract Map<String, SpriteAnimation> getAnimations();
	protected abstract int hitLife();
	protected abstract int dieLife();
	
	protected enum CharacterDirection {
		LEFT, RIGHT
	}
	
	public interface CharacterListener {
		
		public void onChangePosition(int x, int y, CharacterDirection direction);
		
	}
	
}
