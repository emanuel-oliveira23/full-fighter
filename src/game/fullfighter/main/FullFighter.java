package game.fullfighter.main;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class FullFighter extends JFrame {
	
	private static final long serialVersionUID = 1L;

	public FullFighter() {
		initUI();
	}
	
	private void initUI() {
		setContentPane(new Game());
		
		setLocationByPlatform(false);
		setLocation(100, 100);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Full Fighter");
		pack();
		setVisible(true);
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				new FullFighter();
			}
		});
	}
	
}
