package game.fullfighter.main;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;

import game.fullfighter.main.BoundBox.BoundBoxType;

public class Player extends Character {
	
	public Player(int x, int y) {
		super(new Sprite("ss-player.png", 11, 9, 64, 112), x, y);
	}

	@Override
	protected Map<String, SpriteAnimation> getAnimations() {
		Map<String, SpriteAnimation> animations = new HashMap<>();
		
		BoundBox hitBox1 = new BoundBox(45, 35, 32, 79, BoundBoxType.HIT);
		BoundBox attackBox1 = new BoundBox(78, 40, 24, 15, BoundBoxType.ATTACK);
		
		
		animations.put("idle", 
			new SpriteAnimation(0, 0, 2, true, new BoundBox[][]{
				{hitBox1}, {hitBox1}, 
				{hitBox1}}));
		animations.put("walk", 
			new SpriteAnimation(0, 3, 8, true, new BoundBox[][]{
				{hitBox1}, {hitBox1}, 
				{hitBox1}, {hitBox1}, 
				{hitBox1}, {hitBox1}}));
		animations.put("punch",
			new SpriteAnimation(5, 3, 5, false, new BoundBox[][]{
				{hitBox1, attackBox1}, 
				{hitBox1, attackBox1}, 
				{hitBox1, attackBox1}}));
		animations.put("superPunch", 
			new SpriteAnimation(7, 3, 8, false, new BoundBox[][]{{}, {}, 
				{new BoundBox(101, 53, 21, 35, BoundBoxType.ATTACK)}, 
				{new BoundBox(106, 34, 21, 29, BoundBoxType.ATTACK)}, 
				{new BoundBox(72, 0, 21, 33, BoundBoxType.ATTACK)}, 
				{new BoundBox(72, 0, 21, 33, BoundBoxType.ATTACK)}}));
		animations.put("hit",
			new SpriteAnimation(2, 5, 6, false, new BoundBox[][]{}));
		return animations;
	}
	
	@Override
	public void update(Graphics2D g) {
		consumeKeyEvent();
		super.update(g);
	}
	
	@Override
	protected void drawLifeBar(Graphics2D g) {
		g.setColor(Color.WHITE);
		g.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
		g.drawString("Twister", 10, 20);
		
		g.setColor(Color.WHITE);
		g.drawRoundRect(10, 25, 2 * 100, 20, 4, 4);
		g.setColor(Color.decode("#248eff"));
		g.fillRoundRect(10, 25, 2 * getLife(), 20, 4, 4);
	}
	
	@Override
	protected int hitLife() {
		return 5;
	}
	
	@Override
	protected int dieLife() {
		return 25;
	}
	
	@Override
	protected boolean superPunch() {
		if (super.superPunch()) {
			Audio.playResource("player-superPunch.wav");
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	protected boolean punch() {
		if (super.punch()) {
			Audio.playResource("player-punch.wav");
			return true;
		} else {
			return false;
		}
	}
	
	private boolean toogleWalkAudio = false;
	@Override
	protected boolean walk() {
		if (super.walk()) {
			if (!toogleWalkAudio){
				Audio.playResource("player-walk.wav");
				toogleWalkAudio = true;
			} else {
				toogleWalkAudio = false;
			}
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	protected boolean hit() {
		if (super.hit()) {
			Audio.playResource("player-hit.wav");
			return true;
		} else {
			return false;
		}
	}
	
	private void consumeKeyEvent() {
		switch (GameInput.getLastKeyCode()) {
			case KeyEvent.VK_UP:
				moveToUp();
				break;
			case KeyEvent.VK_RIGHT:
				moveToRight();
				break;
			case KeyEvent.VK_DOWN:
				moveToDown();
				break;
			case KeyEvent.VK_LEFT:
				moveToLeft();
				break;
			case KeyEvent.VK_A:
				punch();
				break;
			case KeyEvent.VK_S:
				superPunch();
				break;
			default:
				idle();
		}
	}
	
}
