package game.fullfighter.main;

public class SpriteAnimation {
	
	private int spriteRow;
	private int colStart;
	private int colEnd;
	private int colCurrent;
	private boolean breakbale;
	private BoundBox[][] boundBoxies;
	
	public SpriteAnimation(
		int spriteRow, 
		int colStart, 
		int colEnd, 
		boolean breakbale, 
		BoundBox[][] boundBoxies) {
		
		this.spriteRow = spriteRow;
		this.colStart = colStart;
		this.colEnd = colEnd;
		this.boundBoxies = boundBoxies;
		this.breakbale = breakbale;
		reset();
	}
	
	public int getSpriteRow() {
		return spriteRow;
	}
	
	public int getActualCol() {
		return colCurrent;
	}
	
	public void reset() {
		colCurrent = colStart;
	}
	
	public boolean isEnded() {
		return colCurrent > colEnd;
	}
	
	public boolean isBreakbale() {
		return breakbale;
	}
	
	public boolean canBreak() {
		if (isBreakbale()) {
			return true;
		} else {
			return isEnded();
		}
	}
	
	public void nextCol() {
		colCurrent++;
	}
	
	public BoundBox[] getCurrentBoundBoxies() {
		if (boundBoxies.length > 0) {
			int bbIndex = colCurrent - colStart;
			if (bbIndex >= 0 && bbIndex < boundBoxies.length) {
				return boundBoxies[bbIndex];
			} else {
				return boundBoxies[boundBoxies.length - 1];
			}
		} else {
			return new BoundBox[]{};
		}
	}
	
}
