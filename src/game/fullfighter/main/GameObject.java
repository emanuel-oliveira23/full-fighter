package game.fullfighter.main;

import java.awt.Graphics2D;

public abstract class GameObject {
	
	public void update(Graphics2D g) {}
	
}
