package game.fullfighter.main;

import java.awt.Rectangle;

@SuppressWarnings("serial")
public class BoundBox extends Rectangle {
	
	private BoundBoxType boundBoxType;
	
	public BoundBox(int x, int y, int width, int height, BoundBoxType type) {
		super(x, y, width, height);
		this.boundBoxType = type;
	}
	
	public BoundBoxType getBoundBoxType() {
		return boundBoxType;
	}
	
	public void setBoundBoxType(BoundBoxType boundBoxType) {
		this.boundBoxType = boundBoxType;
	}
	
	public boolean isAttack() {
		return is(BoundBoxType.ATTACK);
	}
	
	public boolean isHit() {
		return is(BoundBoxType.HIT);
	}
	
	public boolean is(BoundBoxType type) {
		return boundBoxType == type;
	}
	
	public enum BoundBoxType {
		HIT, ATTACK
	}
	
}
