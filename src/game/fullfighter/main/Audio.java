package game.fullfighter.main;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import game.fullfighter.res.Resources;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

public class Audio {
	
	
	
	public static AudioStream play(String audioPath) {
		try {
			InputStream in = new FileInputStream(audioPath);
			return play(in);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static AudioStream playResource(String resourceName) {
		return play(Resources.getResourceAsStream(resourceName));
	}
	
	public static AudioStream play(InputStream audioIn) {
		try {
			AudioStream audioStream = new AudioStream(audioIn);
			AudioPlayer.player.start(audioStream);
			return audioStream;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void stop(AudioStream audioIn) {
		AudioPlayer.player.stop(audioIn);
	}
	
}
