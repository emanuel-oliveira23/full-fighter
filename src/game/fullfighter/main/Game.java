package game.fullfighter.main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import game.fullfighter.res.Resources;
import sun.audio.AudioStream;

public class Game extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private static final int frameRate = 8;
	
	private boolean running = true;
	private List<Layer> layers = new ArrayList<Layer>();
	private Player player;
	private PlayerLayer playerLayer;
	private AudioStream mainAudio;
	
	public Game() {
		initGame();
	}
	
	private void initGame() {
		setPreferredSize(new Dimension(Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT));
		setFocusable(true);
		setBackground(Color.BLACK);
		addKeyListener(GameInput.getInstance().getKeyListener());
		
		initLayers();
		initGameLoop();
		playMainAudio();
	}
	
	
	private void initLayers() {
		/* init game background layer */
		Layer backgroundLayer = new Layer();
		GameBackground gameBg = new GameBackground();
		backgroundLayer.addGameObject(gameBg);
		layers.add(backgroundLayer);
		
		/* init player*/
		player = new Player(100, Constants.MAX_CHARACTER_Y);
		player.addCharacterListener(gameBg);
		
		/* init player layer*/
		playerLayer = new PlayerLayer(player);
		layers.add(playerLayer);
		
		/* registring listeners */
		playerLayer.setListener(gameBg);
		gameBg.setListener(playerLayer);
			
	}
	
	public void playMainAudio() {
		mainAudio = Audio.playResource("theme.mid");
	}
	
	public void stopMainAudio() {
		if (mainAudio != null) {
			Audio.stop(mainAudio);
		}
	}
	
	private void initGameLoop() {
		new Thread(new Runnable() {
			
			public void run() {
				while(running) {
					long loopStart = System.currentTimeMillis();
					repaint();
					sleepGameLoop(loopStart);
				}
			}
			
		}).start();
	}
	
	private void sleepGameLoop(long loopStart) {
		long rateTime = 1000 / frameRate;
		long loopTime = System.currentTimeMillis() - loopStart;
		long sleepTime = rateTime - loopTime;
		try { 
			Thread.sleep(sleepTime < 0 ? 0 : sleepTime);
		} catch (InterruptedException e) { 
			e.printStackTrace(); 
		}
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		BufferedImage res = new BufferedImage(
				Constants.SCREEN_WIDTH, 
				Constants.SCREEN_HEIGHT, 
				BufferedImage.TYPE_INT_ARGB);
		Graphics2D gi = (Graphics2D) res.getGraphics();
		
		if (player.died()) {
			paintGameOver(gi);
		} else if ( playerLayer.allEnemiesKilled()) {
			paintYouWin(gi);
		} else {
			for (Layer l : layers) {
				l.draw(gi);
			}
		}
		
		g.drawImage(res, 0, 0, getWidth(), getHeight(), null);
		Toolkit.getDefaultToolkit().sync();
	}
	
	private void paintGameOver(Graphics2D g) {
		g.drawImage(Resources.getImage("game-over.png"), 0, 0, getWidth(), getHeight(), null);
		stopMainAudio();
	}
	
	private void paintYouWin(Graphics2D g) {
		g.drawImage(Resources.getImage("you-win.png"), 
			Constants.SCREEN_WIDTH/2 - 50, 
			Constants.SCREEN_HEIGHT/2 - 82, 
			101, 164, null);
		g.setFont(new Font(Font.SANS_SERIF, Font.ITALIC, 80));
		int x = Constants.SCREEN_WIDTH / 2 - 200;
		int y = Constants.SCREEN_HEIGHT - 30;
		g.setColor(Color.WHITE);
		g.drawString("YOU WIN!", x + 2, y + 2);
		g.setColor(Color.YELLOW);
		g.drawString("YOU WIN!", x, y);
	}
	
}
