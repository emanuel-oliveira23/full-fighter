package game.fullfighter.main;

import java.util.ArrayList;
import java.util.List;

public class StateMachine {
	
	private List<Transaction> transactions = new ArrayList<>();
	private String currentState;
	private OnChangeStateListener onChangeListener;
	
	public StateMachine(String initialState) {
		currentState = initialState;
	}
	
	public String getCurrentState() {
		return currentState;
	}
	
	public void setOnChangeListener(OnChangeStateListener onChangeListener) {
		this.onChangeListener = onChangeListener;
	}
	
	public void addTransaction(String from, String to) {
		transactions.add(new Transaction(from, to));
	}
	
	public boolean to(String targetState) {
		Transaction find = new Transaction(currentState, targetState);
		if (transactions.contains(find)) {
			String oldState = currentState;
			currentState = targetState;
			if (onChangeListener != null) {
				onChangeListener.onChangeState(targetState, oldState);
			}
			return true;
		} else {
			return false;
		}
	}
	
	public static class Transaction {
		
		private String from;
		private String to;
		
		public Transaction(String from, String to) {
			this.from = from;
			this.to = to;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((from == null) ? 0 : from.hashCode());
			result = prime * result + ((to == null) ? 0 : to.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Transaction other = (Transaction) obj;
			if (from == null) {
				if (other.from != null)
					return false;
			} else if (!from.equals(other.from))
				return false;
			if (to == null) {
				if (other.to != null)
					return false;
			} else if (!to.equals(other.to))
				return false;
			return true;
		}
		
		
	}
	
	public static interface OnChangeStateListener {
		public void onChangeState(String newState, String oldState);
	}
	
	
}
