package game.fullfighter.main;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.Map;

import game.fullfighter.main.BoundBox.BoundBoxType;


public class Enemy extends Character {

	public static final int MIN_MOVEMENT_LEVEL = 1;
	public static final int MAX_MOVEMENT_LEVEL = 9;
	public static final int MIN_PUNCH_LEVEL = 1;
	public static final int MAX_PUNCH_LEVEL = 9;
	
	private Player player;
	private int dangerousMovementLevel = 8; 
	private int dangerousPunchLevel = 3; 
	
	public Enemy(int px, int py, int mLevel, int pLevel, Player player) {
		super(new Sprite("ss-enemy.png", 6, 4, 64, 85), px, py);
		
		if (mLevel >= MAX_MOVEMENT_LEVEL) {
			mLevel = MAX_MOVEMENT_LEVEL;
		} else if (mLevel <= MIN_MOVEMENT_LEVEL) {
			mLevel = MIN_MOVEMENT_LEVEL;
		}
		
		if (pLevel >= MAX_PUNCH_LEVEL) {
			pLevel = MAX_PUNCH_LEVEL;
		} else if (pLevel <= MIN_PUNCH_LEVEL) {
			pLevel = MIN_PUNCH_LEVEL;
		}
		
		this.dangerousMovementLevel = pLevel;
		this.dangerousPunchLevel = pLevel;
		this.player = player;
		setCharacterSpeed(DEFAULT_CHARACTER_SPEED - 5);
	}

	@Override
	protected Map<String, SpriteAnimation> getAnimations() {
		Map<String, SpriteAnimation> animations = new HashMap<>();
		
		BoundBox hitBox1 = new BoundBox(43, 15, 28, 61, BoundBoxType.HIT);
		BoundBox attackBox1 = new BoundBox(64, 19, 32, 19, BoundBoxType.ATTACK);
		
		animations.put("idle", 
			new SpriteAnimation(0, 0, 2, true, new BoundBox[][]{{hitBox1}, {hitBox1}, {hitBox1}}));
		animations.put("walk", 
			new SpriteAnimation(0, 1, 2, true, new BoundBox[][]{{hitBox1}, {hitBox1}}));
		animations.put("punch", 
			new SpriteAnimation(5, 0, 0, false, new BoundBox[][]{{hitBox1, attackBox1}}));
		animations.put("hit",
			new SpriteAnimation(1, 1, 3, false, new BoundBox[][]{}));
		animations.put("die",
			new SpriteAnimation(2, 0, 3, false, new BoundBox[][]{}));
		return animations;
	}
	
	@Override
	protected int hitLife() {
		return 25;
	}
	
	@Override
	protected int dieLife() {
		return INITIAL_LIFE / 2;
	}
	
	@Override
	public void update(Graphics2D g) {
		updateDirection();
		makeDecision(g);
		super.update(g);
	}
	
	
	@Override
	protected boolean hit() {
		if (super.hit()) {
			Audio.playResource("enemy-hit.wav");
			return true;
		} else {
			return false;
		}
	}
	
	
	
	private void updateDirection() {
		if (player.getX() < getX()) {
			setDirection(CharacterDirection.LEFT);
		} else {
			setDirection(CharacterDirection.RIGHT);
		}
	}
	
	private void makeDecision(Graphics2D g) {
		boolean doSomething = (Math.random() * MAX_MOVEMENT_LEVEL + 1) <= dangerousMovementLevel;
		if (doSomething) {
			if(isCloseOfPlayer()) {
				doSomething = (Math.random() * MAX_PUNCH_LEVEL + 1) <= dangerousPunchLevel;
				if (doSomething) {
					punch();
				}
			} else {
				move();
			}
		} else {
			idle(); 
		}
	}
	
	private void move() {
		
		int xMovement = player.getX() - getX();
		int yMovement = player.getY() - getY();
		
		if (xMovement < 0) {
			moveToLeft();
		} else {
			moveToRight();
		}
		
		if (yMovement < 0) {
			moveToUp();
		} else {
			moveToDown();
		}
		
		walk();
	}

	private boolean isCloseOfPlayer() {
		return isCloseOf(player);
	}
	
	private boolean isCloseOf(Character character) {
		int heightBox = 10;
		int widthBox = 40;
		
		Rectangle playerTargetBox = new Rectangle(
			character.getX() - widthBox / 2, 
			character.getY() - heightBox, 
			widthBox, 
			heightBox);
		
		Rectangle enemyTargetBox = new Rectangle(
				getX() - widthBox / 2, 
				getY() - heightBox, 
				widthBox, 
				heightBox);
				
		return playerTargetBox.intersects(enemyTargetBox);
	}
	
	
}
