package game.fullfighter.res;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.imageio.ImageIO;

public class Resources {
	
	public static Class<?> clazz = new Resources().getClass();
	
	public static InputStream getResourceAsStream(String resourceName) {
		return clazz.getResourceAsStream(resourceName);
	}
	
	public static URL getResourcePath(String resourceName) {
		return clazz.getResource(resourceName);
	}
	
	public static BufferedImage getImage(String imageName) {
		try {
			return ImageIO.read(getResourceAsStream(imageName));
		} catch (IOException ex) {
			return new BufferedImage(0, 0, BufferedImage.TYPE_INT_ARGB);
		}
	}
	
}
